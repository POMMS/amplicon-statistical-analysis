# Build
Pushing to gitlab automatically builds the image.  Details are at <https://gitlab.oit.duke.edu/OIT-DCC/singularity-example>


# Download Image
Image can be downloaded with this command

```
curl -O https://research-singularity-registry.oit.duke.edu/POMMS/amplicon-statistical-analysis.sif
```

# Running
## Basic
```
singularity run amplicon-statistical-analysis.sif
```

## Bind Mount Host Directories
The following is an example, host directory paths need to be customized
```
BASE_DIR="/work/josh/pomms"
DATA_SRC="$BASE_DIR/rawdata"
WORKSPACE_SRC="$BASE_DIR/workspace"
mkdir -p $DATA_SRC $WORKSPACE_SRC

singularity run --bind ${DATA_SRC}:/data --bind ${WORKSPACE_SRC}:/workspace amplicon-statistical-analysis.sif
```

## SSH Tunneling

echo "DCC Node:" `hostname -A`
echo "port tunneling command (replace PORT with appropriate value and remove space before colon) "
echo "ssh -L PORT:`hostname -A`:PORT dcc-slogin.oit.duke.edu"

